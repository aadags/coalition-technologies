<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Route::group(['middleware' => ['api.usage']], function () {  //log each usage
    
    /*
     * APIs
     */
    Route::group(['prefix' => 'api/v1'], function () {


        Route::get('/data/retrieve', [
            'as'   => 'dataRetriever',
            'uses' => 'ApiController@dataRetriever',
        ]);

        Route::get('/data/single/retrieve/{id}', [
            'as'   => 'singleDataRetriever',
            'uses' => 'ApiController@singleDataRetriever',
        ]);

        Route::post('/data/save', [
            'as'   => 'dataSaver',
            'uses' => 'ApiController@dataSaver',
        ]);
        
        Route::post('/data/single/update', [
            'as'   => 'dataUpdater',
            'uses' => 'ApiController@dataUpdater',
        ]);


    });

//});