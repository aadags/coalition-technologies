<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    </head>
    <body>
                <div class="container">
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <h2 class="card-title text-center">Laravel Skills Test v1.0</h2>
                                <div class="card-body">
                                    <form  method="POST" id="coalitionForm" action="{{ route('dataSaver') }}">                                              
                                    {{ csrf_field() }}                                      
                                    <div class="row">
                                        <div class="col-md-12 mr-auto">
                                                <div id="response"></div>
                                                <div class="form-group has-default">
                                                    @if ($errors->has('name'))
                                                        <span class="help-block text-danger">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="material-icons">label_important</i>
                                                            </span>
                                                        </div>
                                                        <input type="text" name="name" required="true" class="form-control" placeholder="Product Name" value="{{ old('name') }}">
                                                    </div>
                                                </div>
                                                
                                        </div>
                                        <div class="col-md-6">
                                                    
                                                <div class="form-group has-default">
                                                    @if ($errors->has('quantity'))
                                                        <span class="help-block text-danger">
                                                            <strong>{{ $errors->first('quantity') }}</strong>
                                                        </span>
                                                    @endif
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="material-icons">label_important</i>
                                                            </span>
                                                        </div>
                                                        <input type="number" name="quantity" required="true" class="form-control" placeholder="Quantity in stock" value="{{ old('quantity') }}">
                                                    </div>
                                                </div>
                                              
                                                
                                        </div>
                                        <div class="col-md-6">
                                                    
                                            <div class="form-group has-default">
                                                @if ($errors->has('price'))
                                                    <span class="help-block text-danger">
                                                        <strong>{{ $errors->first('price') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="material-icons">attach_money</i>
                                                        </span>
                                                    </div>
                                                    <input type="number" name="price" required="true" class="form-control" placeholder="Price per item" value="{{ old('price') }}">
                                                </div>
                                            </div>
                                          
                                            
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <br/>    
                                            <button type="submit" class="save btn btn-success btn-md">Save</button>
                                            <span class="saving" style="display:none;"><i class="fa fa-spinner fa-spin"></i> Your data is submitting .....</span>
                                        </div>
                                    </div>
                                </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>

                    <div class="row">
                        <div class="col-md-12">
                        <div class="db-list text-center">
                            <h3>List of data <button type="button" class="btn btn-primary btn-md" onclick="getDataList()">Refresh</button>
                                <span class="loading" style="display:none;"><i class="fa fa-spinner fa-spin"></i></span></h3>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                              <th>#</th>
                                              <th>Product name</th>
                                              <th>Quantity in stock</th>
                                              <th>Price per item</th>
                                              <th>Datetime submitted</th>
                                              <th>Total value number</th>
                                              <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="data-list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="row">&nbsp;</div>

                </div>

                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title text-red">Edit Data</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <hr style="margin:10px 0px -5px;">
                            <div class="modal-body">
                                    <form  method="POST" id="coalitionForm2" action="{{ route('dataUpdater') }}">   
                                            <input type="hidden" name="id" id="id">

                                            {{ csrf_field() }}                                      
                                            <div class="row">
                                                <div class="col-md-12 mr-auto">
                                                        <div id="response2"></div>
                                                        <div class="form-group has-default">
                                                            @if ($errors->has('name'))
                                                                <span class="help-block text-danger">
                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                </span>
                                                            @endif
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="material-icons">label_important</i>
                                                                    </span>
                                                                </div>
                                                                <input type="text" name="name" id="name" required="true" class="form-control" placeholder="Product Name" value="{{ old('name') }}">
                                                            </div>
                                                        </div>
                                                        
                                                </div>
                                                <div class="col-md-6">
                                                            
                                                        <div class="form-group has-default">
                                                            @if ($errors->has('quantity'))
                                                                <span class="help-block text-danger">
                                                                    <strong>{{ $errors->first('quantity') }}</strong>
                                                                </span>
                                                            @endif
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="material-icons">label_important</i>
                                                                    </span>
                                                                </div>
                                                                <input type="number" name="quantity" id="quantity" required="true" class="form-control" placeholder="Quantity in stock" value="{{ old('quantity') }}">
                                                            </div>
                                                        </div>
                                                      
                                                        
                                                </div>
                                                <div class="col-md-6">
                                                            
                                                    <div class="form-group has-default">
                                                        @if ($errors->has('price'))
                                                            <span class="help-block text-danger">
                                                                <strong>{{ $errors->first('price') }}</strong>
                                                            </span>
                                                        @endif
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="material-icons">attach_money</i>
                                                                </span>
                                                            </div>
                                                            <input type="number" name="price" id="price" required="true" class="form-control" placeholder="Price per item" value="{{ old('price') }}">
                                                        </div>
                                                    </div>
                                                  
                                                    
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <br/>    
                                                    <button type="submit" class="save btn btn-success btn-md">Save</button>
                                                    <span class="saving" style="display:none;"><i class="fa fa-spinner fa-spin"></i> Your data is submitting .....</span>
                                                </div>
                                            </div>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>                

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script> 
    <script src="/js/jquery.form.min.js"></script> 
    <script src="/js/formSubmit.js"></script> 
    </body>
</html>
