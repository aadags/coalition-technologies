## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

What things you need to install the software


```
PHP 7.1 [LTS]
```

```
Laravel 5.5 [LTS]
```

Laravel 5.5 was used because its the last LTS  for laravel and supports the LTS for PHP 7.1

### Installing

A step by step series of examples that tell you have to get the software running

Step 1

```
git clone https://bitbucket.org/aadags/coalition-technologies.git
```

Step 2

```
cd to/project/folder
```
```
composer install
```

Step 3

```
php artisan serve --port=8080
```
Other commands to run

```
php -r \"file_exists('.env') || copy('.env.example', '.env');\"
```
```
php artisan key:generate
```
```
php artisan package:discover

```

## Running the tests

Run all test coverage using phpunit.  
For linux bash
```
vendor/bin/phpunit
```
For windows cmd
```
vendor\bin\phpunit
```



## Deployment

This app can be deployed to a dockerized container using dokku 
