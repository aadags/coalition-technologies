<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Validator; 
use Log;

class ApiController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Api Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles api requets for the application.
    |
    */

        /**
     * Post to save data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataSaver(Request $request) {

        try{
    
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'quantity' => 'required|numeric',
                'price' => 'required|numeric'
            ]);
    
            if ($validator->fails()) {
                $message = null;
                foreach ($validator->errors() as $error){
                    $message .= $error.'. \r\n ';
                }
                return response()->json(['message' => $message], 422);
            }
    
            $dir = public_path('db');
            if(!is_dir($dir)){
                mkdir($dir);
            }
    
            $prevData = [];
    
            if(is_file($dir.'/data.json')){
                $file = file_get_contents($dir.'/data.json');
                $prevData = json_decode($file, true);
            }
    
            $count = sizeof($prevData);
    
            $data = [];        
            $data[0] = $request->except('_token');
            $data[0]['created_at'] = date('Y-m-d h:i:s', time());
            $data[0]['updated_at'] = date('Y-m-d h:i:s', time());
    
            if($count > 0){
               $prevData[$count] = $data[0];
               $data = $prevData;
            }
    
            $content = json_encode($data);
    
            $file = fopen($dir.'/data.json','w');
            fwrite($file,$content);
            fclose($file);
    
            return response()->json(['message' => 'Your data is saved successfully'], 200);  
    
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            
            return response()->json(['message' => 'Oops!! Something went wrong, Try again.'], 500); 
        } 
            
    }


            /**
     * Post to save data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataUpdater(Request $request) {

        try{
    
            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
                'name' => 'required|string',
                'quantity' => 'required|numeric',
                'price' => 'required|numeric'
            ]);
    
            if ($validator->fails()) {
                $message = null;
                foreach ($validator->errors() as $error){
                    $message .= $error.'. \r\n ';
                }
                return response()->json(['message' => $message], 422);
            }
    
            $data = [];
            $rquest = $request->except('_token');

            $dir = public_path('db');

            if(is_file($dir.'/data.json')){
                $file = file_get_contents($dir.'/data.json');
                $data = json_decode($file, true);
            }

            if(sizeof($data) <= $rquest['id']){
                return response()->json(['message' => 'Data does not exist!'], 404);    
            }

            $data[$rquest['id']]['name'] = $rquest['name'];
            $data[$rquest['id']]['price'] = $rquest['price'];
            $data[$rquest['id']]['quantity'] = $rquest['quantity'];
            $data[$rquest['id']]['updated_at'] = date('Y-m-d h:i:s', time());

            $content = json_encode($data);
    
            $file = fopen($dir.'/data.json','w');
            fwrite($file,$content);
            fclose($file);
    
            return response()->json(['message' => 'Your data is saved successfully'], 200);  
    
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            
            return response()->json(['message' => 'Oops!! Something went wrong, Try again.'], 500); 
        } 
            
    }
    

    /**
     * Get the list of data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataRetriever(Request $request) {

        try{

        $dir = public_path('db');

        $data = [];

        if(is_file($dir.'/data.json')){
            $file = file_get_contents($dir.'/data.json');
            $data = json_decode($file, true);
        }
           
        return response()->json(['message' => 'Success', 'body' => $data], 200);  
        
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            
            return response()->json(['message' => 'Oops!! Something went wrong, Try again.'], 500);    
        } 
 
    }

    public function singleDataRetriever(Request $request, $id) {

        try{

        $dir = public_path('db');

        $data = [];

        if(is_file($dir.'/data.json')){
            $file = file_get_contents($dir.'/data.json');
            $data = json_decode($file, true);
        }

        if(sizeof($data) <= $id){
            return response()->json(['message' => 'Data does not exist!'], 404);    
        }

        $data = $data[$id];
           
        return response()->json(['message' => 'Success', 'body' => $data], 200);  
        
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            
            return response()->json(['message' => 'Oops!! Something went wrong, Try again.'], 500);    
        } 
 
    }
    
    
}
