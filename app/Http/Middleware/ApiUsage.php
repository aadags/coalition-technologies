<?php

namespace App\Http\Middleware;

use Closure;

class ApiUsage
{
    /**
     * log the usage of an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //log usage here
        return $next($request);
    }
}