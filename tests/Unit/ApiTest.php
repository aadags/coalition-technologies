<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testDataRetriever()
    {
        $response = $this->json('GET', '/api/v1/data/retrieve');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'body' => []
            ]);
    }

    /**
     *
     * @return void
     */
    public function testSingleDataRetriever()
    {
        $response = $this->json('GET', '/api/v1/data/single/retrieve/0');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'body'
            ]);
    }

    /**
     *
     * @return void
     */
    public function testPostdataSaver()
    {
        $response = $this->json('POST', '/api/v1/data/save', ['name' => 'sample', 'quantity' => 5, 'price'=> '1000']);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }


    public function testPostdataUpdater()
    {
        $response = $this->json('POST', '/api/v1/data/single/update', ['id' => 0, 'name' => 'sample', 'quantity' => 5, 'price'=> 1000]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
    
}
