
$(document).ready(function() {
	getDataList(); 
	
	var options = {
			target:   '#dataBody',   // target element(s) to be updated with server response
			dataType: 'json',
			success:      function(responseText) {
						
									$("#response").html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ responseText.message +'</div>');
									$('button.save').show();
									$('span.saving').hide();
									getDataList();
								
		 	},  
			error:      function(responseText){
							$("#response").html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ responseText.responseJSON.message +'</div>');	
							$('button.save').show();
							$('span.saving').hide();
						}, 
			resetForm: true        // reset the form after successful submit
		};

	 $('#coalitionForm').submit(function() {
		$('button.save').hide();
			$('span.saving').show();
			$(this).ajaxSubmit(options);
			// always return false to prevent standard browser submit and page navigation
			return false;
	 });

	 var options2 = {
		target:   '#dataBody',   // target element(s) to be updated with server response
		dataType: 'json',
		success:      function(responseText) {
					
								$("#response2").html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ responseText.message +'</div>');
								$('button.save').show();
								$('span.saving').hide();
								$('#editModal').modal('hide');
								getDataList();
							
		 },  
		error:      function(responseText){
						$("#response2").html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ responseText.responseJSON.message +'</div>');	
						$('button.save').show();
						$('span.saving').hide();
					}, 
		resetForm: true        // reset the form after successful submit
	};

	 $('#coalitionForm2').submit(function() {
		$('button.save').hide();
			$('span.saving').show();
			$(this).ajaxSubmit(options2);
			// always return false to prevent standard browser submit and page navigation
			return false;
	 });
});

function getDataList(){	  
	$('span.loading').show();
	$.get("/api/v1/data/retrieve", function (response) {

		var products = response.body;
		var dataBody = '';

		if(typeof products != 'undefined' && products != null && products.length != null && products.length > 0) {
			for (let i = 0; i < products.length; i++){
				dataBody += '<tr>';
				dataBody += '<td>' + (i+1) + '</td>';
				dataBody += '<td>' + products[i]['name'] + '</td>';
				dataBody += '<td>' + products[i]['quantity'] + '</td>';
				dataBody += '<td>$' + products[i]['price'] + '</td>';
				dataBody += '<td>' + products[i]['created_at'] + '</td>';
				dataBody += '<td>$' + (products[i]['quantity'] * products[i]['price']) + '</td>';
				dataBody += '<td><button type="button" class="btn btn-primary btn-md" onclick="editData('+ i +')">Edit</button></td>';
				dataBody += '</tr>';
			}
		} else {
			dataBody = '<tr><td colspan="7">No data found</td></tr>';
		}

		$('tbody.data-list').html(dataBody);
		$('span.loading').hide();

	  }).fail(function(error) {
		alert( error.responseJSON.message );
	});
}

function editData(id){	  
	$.get("/api/v1/data/single/retrieve/"+id, function (response) {

		var product = response.body; 		

		$('#id').val(id);
		$('#name').val(product.name);
		$('#quantity').val(product.quantity);
		$('#price').val(product.price);
		$('#editModal').modal('show');

	  }).fail(function(error) {
		alert( error.responseJSON.message );
	});
}